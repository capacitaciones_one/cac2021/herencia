package entidades;

public class Animal {

    String nombre;
    int edad;
    String color;
    double peso;

    public Animal(String nombre, int edad, String color, double peso) {
        this.nombre = nombre;
        this.edad = edad;
        this.color = color;
        this.peso = peso;
    }

    public void hablar() {
        System.out.println("Yo hablo segun el animal que fuere!");
    }

    @Override
    public String toString() {
        return "Animal{" + "nombre=" + nombre + ", edad=" + edad + ", color=" + color + ", peso=" + peso + '}';
    }

}
