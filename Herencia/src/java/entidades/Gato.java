package entidades;

public class Gato extends Animal{


    public Gato(String nombre, int edad, String color, double peso) {
        super(nombre, edad, color, peso);
    }

    @Override
    public void hablar() {
        System.out.println("No, para. Hablo como quiero! Miau!!!");
    }
    
    

}
