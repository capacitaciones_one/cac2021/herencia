package test;

import entidades.Animal;
import entidades.Gato;
import entidades.Perro;

public class AnimalesTest {
    public static void main(String[] args) {
        System.out.println("[...]");
        Gato miGato = new Gato("Garfield", 40, "Naranja", 80);
        Perro miPerro = new Perro("Rufus", 3, "Blanco", 10);
        System.out.println(miGato);
        System.out.println(miPerro);
        
        miGato.hablar();
        miPerro.hablar();
        System.out.println("[OK]");
    }
    
}
